<?php

//pour utiliser les variables de session
//session_start();

use gamepedia\controllers\GameController;
use gamepedia\controllers\CompanyController;
use gamepedia\controllers\PlatformController;
use gamepedia\models\Usr;
use gamepedia\models\Game;
use gamepedia\models\Character;
use gamepedia\models\Platform;
use gamepedia\models\Com2usr;
use gamepedia\models\Commentaires;

require 'vendor/autoload.php';
require_once 'vendor/fzaninotto/faker/src/autoload.php';

\conf\Eloquent::init('src/conf/conf.ini');

set_time_limit(300);

$app = new \Slim\Slim;





$app->get('/api/games/:id',function($id) use($app){
    $app->response->headers->set('Content-Type', 'application/json');
    $res = Game::giveGameId($id);
    $tab['game']=$res;
    $tab['links']=array("comments"=>array("href"=>"/api/games/$id/comments"),"characters"=>array("href"=>"/api/games/$id/comments"));


    $tmp = Game::givePlatforms($id);
    foreach($tmp as $platform){
        $links["href"] ="/api/platform/$platform->id";
        $platform["links"]=$links;
    }
    $tab['platforms']=$tmp;

    echo json_encode($tab,JSON_UNESCAPED_SLASHES);


});


$app->get('/api/games',function() use($app){
    $app->response->headers->set('Content-Type', 'application/json');
    if($app->request()->params('page') != null){
        $page = $app->request()->params('page');
        $res = Game::giveGamesNumber(($page-1)*200+1,($page-1)*200+200);
        $json['games'] = $res;

        $prev = $page-1;
        $next = $page+1;

        $links = array("prev" =>array("href"=>"/api/games?page=$prev"), "next" =>  array("href"=>"/api/games?page=$next"));

        $json['links']= $links;
        echo json_encode($json,JSON_UNESCAPED_SLASHES);
    }else{
        $res =Game::giveGamesNumber(1,200);
        $json['games'] = $res;
        echo json_encode($json,JSON_UNESCAPED_SLASHES);
    }


});


$app->get('/api/games/:id/comments',function($id) use($app){
    $app->response->headers->set('Content-Type', 'application/json');
    $comments = Commentaires::giveComments($id);
    echo json_encode($comments);


});


$app->get('/api/games/:id/characters',function($id) use($app){
    $app->response->headers->set('Content-Type', 'application/json');
    $characters = Game::giveCharacters($id);
    echo json_encode($characters,JSON_UNESCAPED_SLASHES);
});

$app->get('/api/platform/:id',function($id) use($app){
    $app->response->headers->set('Content-Type', 'application/json');
    $plat = Platform::giveDescr($id);
    echo json_encode($plat,JSON_UNESCAPED_SLASHES);
});


$app->get('/', function(){


    //tous les jeux
    //$con0 = new GameController();
    //$con0->printAllGames();

    //jeu avec Mario
    //$con1 = new GameController();
    //$con1->printGames('Mario');


    //compagnies japonaises
    //$con2 = new CompanyController();
    //$con2->printCompanies('Japan');

    //platform avec 10 000 000
    //$con3 = new PlatformController();
    //$con3->printPlatforms(10000000);


    //jeu rank
    //$con4 = new GameController();
    //$con4->printGameRank(442,21173);


    //jeu paginate
    //$con5 = new GameController();
    //$con5->printGamePaginate();

    //characters pour un jeu donné
    //$con6 = new GameController();
    //$con6->printGameCharacters(12342);

    //characters pour un nom contenant blabla
    //$con7 = new GameController();
    //$con7->printCharactersGame('Mario');



    //jeux dev par compagnie comportant blabla
    //$con8 = new CompanyController();
    //$con8->printGamesCompanies('Sony');

    //characters pour un jeu contenant blabla et ayant plus de 3 personnages
    //$con9 = new GameController();
    //$con9->print3moreCharactersGame('Mario');


    //jeux et leurs ratings contenant 'Mario'
    //$con10 = new GameController();
    //$con10->printRatings('Mario');

    //jeux et leurs ratings contenant 'Mario' et dont le rating initial contient 3+
    //$con11 = new GameController();
    //$con11->printRatings3('Mario');

    //jeux,developpeurs et leurs ratings contenant 'Mario' et dont le rating initial contient 3+ et dont la compagnie contient 'Inc'
    //$con12 = new GameController();
    //$con12->printRatings3AndCompLike('Mario');


});


function script()
{

    $fake = Faker\Factory::create('fr_FR');

    $id_game = 12342;
    for ($i = 0; $i < 2; $i++) {
        $name = $fake->firstName;
        $forename = $fake->firstName;
        $email = $fake->email;
        $adress = $fake->address;
        $phonenumber = $fake->phoneNumber;
        $birthdate = $fake->date('Y-m-d', 'now');

        $usr = new Usr();
        $usr->name = $name;
        $usr->forname = $forename;
        $usr->email = $email;
        $usr->adress = $adress;
        $usr->phonenumber = $phonenumber;
        $usr->birthdate = $birthdate;
        $usr->save();

        for ($j = 0; $j < 3; $j++) {
            $titre = $fake->title;
            $contenu = $fake->realText(200, 2);

            $commentaire = new Commentaires();
            $commentaire->titre = $titre;
            $commentaire->contenu = $contenu;
            $commentaire->email = $email;
            $commentaire->id_game = $id_game;
            $commentaire->save();
        }


    }
}

    function script2()
    {
        set_time_limit(300);
        $fake = Faker\Factory::create('fr_FR');

        for ($i = 0; $i < 25000; $i++) {
            $name = $fake->firstName;
            $forename = $fake->firstName;
            $email = $fake->email;
            $adress = $fake->address;
            $phonenumber = $fake->phoneNumber;
            $birthdate = $fake->date('Y-m-d', 'now');

            $usr = new Usr();
            $usr->name = $name;
            $usr->forname = $forename;
            $usr->email = $email;
            $usr->adress = $adress;
            $usr->phonenumber = $phonenumber;
            $usr->birthdate = $birthdate;
            $usr->save();



            for ($j = 0; $j < 250000; $j++) {

                $titre = $fake->title;
                $contenu = $fake->realText(200, 2);

                $commentaire = new Commentaires();
                $commentaire->titre = $titre;
                $commentaire->contenu = $contenu;
                $commentaire->email = $email;
                $commentaire->id_game = rand(1,Game::countGame());
                $commentaire->save();
            }

        }



    }


    $app->run();








