<?php

namespace gamepedia\controllers;


use gamepedia\models\Platform;
/**
 * Created by PhpStorm.
 * User: Quentin
 * Date: 27/02/2017
 * Time: 17:03
 */
class PlatformController
{

    public function printPlatforms($num)
    {

        $platforms= Platform::givePlatforms($num);
        $res = <<<END
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet"   href="css.css">
    <title>photobox</title>
</head><body><ul>
END;
        foreach($platforms as $platform){
            $res.= '<li>'.$platform->name.'</li>';
        }

        $res .='</ul></body></html>';
        echo $res;


    }
}