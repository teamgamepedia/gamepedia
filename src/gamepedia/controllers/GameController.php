<?php

namespace gamepedia\controllers;


use gamepedia\models\Game;
/**
 * Created by PhpStorm.
 * User: Quentin
 * Date: 27/02/2017
 * Time: 17:03
 */
class GameController
{

    public function printAllGames(){
        $time_debut = microtime(true);
        $games = Game::giveAllGames();
        $time_fin = microtime(true);
        $time_exec = $time_fin-$time_debut ;
        $res = <<<END
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet"   href="css.css">
    <title>photobox</title>
</head><body><ul>
END;
        $i = 1 ;
        foreach($games as $game){
            $res.= '<li>'.$i.'-'.$game->name.'</li>';
            $i ++ ;
        }
        $res.='<p>'.$time_exec.'</p>';
        $res .='</ul></body></html>';
        echo $res;
    }

    public function printGames($name)
    {
        $time_debut = microtime(true);
        $games = Game::giveGame($name);
        $time_fin = microtime(true);
        $time_exec = $time_fin-$time_debut ;
        $res = <<<END
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet"   href="css.css">
    <title>photobox</title>
</head><body><ul>
END;
        $i = 1 ;
        foreach($games as $game){
            $res.= '<li>'.$i.'-'.$game->name.'</li>';
            $i ++ ;
        }
        $res.='<p>'.$time_exec.'</p>';
        $res .='</ul></body></html>';
        echo $res;


    }

    public function printGameRank($ngame,$rank)
    {

        $games = Game::giveGameRank($ngame,$rank);
        $res = <<<END
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet"   href="css.css">
    <title>photobox</title>
</head><body><ul>
END;
        foreach($games as $game){
            $res.= '<li>'.$game->name.'</li>';
        }

        $res .='</ul></body></html>';
        echo $res;


    }

    public function printGamePaginate()
    {

        $games = Game::giveGamePaginate();
        $res = <<<END
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet"   href="css.css">
    <title>photobox</title>
</head><body><ul>
END;
        foreach($games as $game){
            $res.= '<li>'.$game->name.'<br>'.$game->deck.'</li>';
        }


        $res .='</ul></body>';
        echo $res;
        echo $games->render().'</html>';


    }




    public function printGameCharacters($idgame)
    {

        $characters = Game::giveCharacters($idgame);
        $res = <<<END
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet"   href="css.css">
    <title>photobox</title>
</head><body><ul>
END;
        foreach($characters as $character){

            $res.= '<li>'.$character->name.'<br>'.$character->deck.'</li>';
        }
        $res .='</ul></body></html>';
        echo $res;
    }

    public function printCharactersGame($name)
    {

        $games = Game::giveGame2($name);
        $res = <<<END
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet"   href="css.css">
    <title>photobox</title>
</head><body><ul>
END;
        foreach($games as $game){
            $characters = $game->characters;
            $res .='<h2>'.$game->name.'</h2>';
            foreach($characters as $character){
                $res.= '<li>'.$character->name.'<br>'.$character->deck.'</li>';
            }

        }
        $res .='</ul></body></html>';
        echo $res;
    }

    public function print3moreCharactersGame($name)
    {

        $games = Game::giveGame2($name);
        $res = <<<END
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet"   href="css.css">
    <title>photobox</title>
</head><body><ul>
END;
        foreach($games as $game){
            $characters = $game->characters;
            if ($characters->count()>3) {
                $res .= '<h2>' . $game->name . '</h2>';
                foreach ($characters as $character) {
                    $res .= '<li>' . $character->name . '<br>' . $character->deck . '</li>';
                }
            }
        }
        $res .='</ul></body></html>';
        echo $res;
    }

    public function printRatings($name){
        $time_debut = microtime(true);
        $games = Game::giveGame($name);
        $time_fin = microtime(true);
        $time_exec = $time_fin-$time_debut ;
        $res = <<<END
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet"   href="css.css">
    <title>photobox</title>
</head><body><ul>
END;
        $i = 1 ;
        foreach($games as $game){
            $ratings = Game::giveRatings($game->id);
            $res.= '<li>'.$i.'-'.$game->name.'</li>';
            $res.='<ul>';
            foreach ($ratings as $rating){
                $res.='<li>'.$rating->name.'</li>';
            }
            $res.='</ul>';
            $i ++ ;

        }
        //$res.='<p>'.$time_exec.'</p>';
        $res .='</ul></body></html>';
        echo $res;
    }




    public function printRatings3($name){
        $time_debut = microtime(true);
        $games = Game::giveGame($name);
        $time_fin = microtime(true);
        $time_exec = $time_fin-$time_debut ;
        $res = <<<END
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet"   href="css.css">
    <title>photobox</title>
</head><body><ul>
END;
        $i = 1 ;
        foreach($games as $game){
            $ratings = Game::giveRatings($game->id);
            foreach ($ratings as $rating){
                if(strstr($rating->name,'3+')){
                    $res.= '<li>'.$i.'-'.$game->name.'</li>';
                    $res.='<ul>';
                    foreach ($ratings as $rating){
                        $res.='<li>'.$rating->name.'</li>';
                    }
                    $res.='</ul>';
                    $i ++ ;
                    break;
                }
            }

        }
        //$res.='<p>'.$time_exec.'</p>';
        $res .='</ul></body></html>';
        echo $res;
    }

    public function printRatings3AndCompLike($name){
        $time_debut = microtime(true);
        $games = Game::giveGame($name);
        $time_fin = microtime(true);
        $time_exec = $time_fin-$time_debut ;
        $res = <<<END
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet"   href="css.css">
    <title>photobox</title>
</head><body><ul>
END;
        $i = 1 ;
        foreach($games as $game){
            $ratings = Game::giveRatings($game->id);
            $developpers = Game::giveDeveloppers($game->id);
            foreach ($ratings as $rating) {
                foreach ($developpers as $developper) {
                    if (strstr($rating->name, '3+') && strstr($developper->name, 'Inc')) {
                        $res .= '<li>' . $i . '-' . $game->name . '</li>';
                        $res .= '<ul>';
                        foreach ($developpers as $developper) {
                            $res .= '<li>' . $developper->name . '</li>';
                        }
                        $res .= '</ul>';
                        $res .= '<ul>';
                        foreach ($ratings as $rating) {
                            $res .= '<li>' . $rating->name . '</li>';
                        }
                        $res .= '</ul>';
                        $i++;
                        break;
                    }
                }
            }
        }
        $res .='</ul></body></html>';
        $time_fin = microtime(true);
         $time_exec = $time_fin-$time_debut ;
         echo $time_exec;
        echo $res;

    }

}