<?php

namespace gamepedia\controllers;


use gamepedia\models\Company;
use gamepedia\models\Game;
/**
 * Created by PhpStorm.
 * User: Quentin
 * Date: 27/02/2017
 * Time: 17:03
 */
class CompanyController
{

    public function printCompanies($name)
    {

        $companies = Company::giveCompanies($name);
        $res = <<<END
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet"   href="css.css">
    <title>photobox</title>
</head><body><ul>
END;
        foreach($companies as $company){
            $res.= '<li>'.$company->name.'</li>';
        }

        $res .='</ul></body></html>';
        echo $res;

    }


    public function printGamesCompanies($name)
    {

        $companies = Company::giveCompaniesLike($name);


        $res = <<<END
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet"   href="css.css">
    <title>photobox</title>
</head><body><ul>
END;
        foreach($companies as $companie){

            $games = $companie->giveGames($companie->id);
            $res .='<h2>'.$companie->name.'</h2>';
            foreach($games as $game){
                $res.= '<li>'.$game->name.'<br>'.$game->deck.'</li>';
            }
        }
        $res .='</ul></body></html>';
        echo $res;


    }
}