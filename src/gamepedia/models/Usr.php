<?php


namespace gamepedia\models;

/**
 * Created by PhpStorm.
 * User: Quentin
 * Date: 27/02/2017
 * Time: 16:45
 */
class Usr extends \Illuminate\Database\Eloquent\Model
{

    protected $table = "usr";
    protected $primaryKey = "email";
    public $timestamps = false;


    public function commentaires(){
        return $this->hasMany('gamepedia\models\Commentaires');
    }






}