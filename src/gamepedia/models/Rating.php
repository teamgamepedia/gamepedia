<?php
/**
 * Created by PhpStorm.
 * User: Quentin
 * Date: 06/03/2017
 * Time: 17:01
 */

namespace gamepedia\models;


class Rating extends \Illuminate\Database\Eloquent\Model
{

    protected $table = "game_rating";
    protected $primaryKey = "id";
    public $timestamps = false;


    public function games(){
        return $this->belongsToMany('\gamepedia\models\Game','game2rating','rating_id','game_id');
    }

}