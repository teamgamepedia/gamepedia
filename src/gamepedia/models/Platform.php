<?php

namespace gamepedia\models;

/**
 * Created by PhpStorm.
 * User: Quentin
 * Date: 27/02/2017
 * Time: 16:45
 */
class Platform extends \Illuminate\Database\Eloquent\Model
{
    protected $table = "platform";
    protected $primaryKey = "id";
    public $timestamps = false;


    public function givePlatforms($minimalInstallBase){
    	return $InstallBase = Platform::where('install_base','>=',$minimalInstallBase)->get();
    }

    public function giveDescr($id){
        return Platform::select('description')->where('id','=',$id)->first();
    }


    public function games(){
        return $this->belongsToMany('\gamepedia\models\Platform','game2platform','platform_id','game_id');
    }
}