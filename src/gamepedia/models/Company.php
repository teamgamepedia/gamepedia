<?php


namespace gamepedia\models;

/**
 * Created by PhpStorm.
 * User: Quentin
 * Date: 27/02/2017
 * Time: 16:45
 */
class Company extends \Illuminate\Database\Eloquent\Model
{

    protected $table = "company";
    protected $primaryKey = "id";
    public $timestamps = false;

    public function giveCompanies($country){
        return $companies = Company::where('location_country','=',$country)->get();
    }

    public function giveCompaniesLike($name){
        return $companies = Company:: where('name','LIKE','%'.$name.' %')->get();
    }

    public function developedGames(){
        return $this->belongsToMany('\gamepedia\models\Game','game_developers','comp_id','game_id');
    }

    public function giveGames($idcompany){
        $c = Company::find($idcompany);
        return $c->developedGames()->get();
    }




}