<?php


namespace gamepedia\models;
use Illuminate\Support\Facades\DB;

/**
 * Created by PhpStorm.
 * User: Quentin
 * Date: 27/02/2017
 * Time: 16:37
 */
class Game  extends \Illuminate\Database\Eloquent\Model
{

    protected $table = "game";
    protected $primaryKey = "id";
    public $timestamps = false;


    public function giveAllGames(){
        return $games = Game::all()->get();
    }

    public static function giveGamesNumber($number1,$number2){
        return $games = Game::whereBetween('id',[$number1,$number2])->get();
    }

    public function giveGame($name){

    	return $games = Game:: where('name','LIKE','%'.$name.' %')->get();
    }

    public static function giveGameId($id){

        return $games = Game:: select('id','name','alias','deck','description','original_release_date')->where('id','=',$id)->first();
    }

    public function giveGame2($name){
        return $games = Game:: where('name','LIKE',$name.' %')->get();
    }

    public function giveGameRank($ngame, $rank){
        return $games = Game::whereBetween('id',array($rank,$rank+$ngame))->get();
    }

    public function giveGamePaginate(){
        $games = Game::select('name','deck')->paginate(500);
        return $games ;
    }

    public function characters(){
        return $this->belongsToMany('\gamepedia\models\Character','game2character','game_id','character_id');
    }

    public function giveCharacters($idgame){
        $g = Game::find($idgame);
        return $g->characters()->get();
    }

    public function developpers(){
        return $this->belongsToMany('\gamepedia\models\Company','game_developers','game_id','comp_id');
    }

    public function giveDeveloppers($idGame){
        $g = Game::find($idGame);
        return $g->developpers()->get();
    }

    public function ratings(){
        return $this->belongsToMany('\gamepedia\models\Rating','game2rating','game_id','rating_id');
    }

    public function giveRatings($idGame){
        $g = Game::find($idGame);
        return $g->ratings()->get();
    }

    public static function countGame(){
        return Game::get()->count();
    }

    public function platforms(){
        return $this->belongsToMany('\gamepedia\models\Platform','game2platform','game_id','platform_id');
    }

    public static function givePlatforms($idgame){
        $g = Game::find($idgame);
        return $g->platforms()->select('id','name','alias','abbreviation')->get();
    }

}