<?php

namespace gamepedia\models;

/**
 * Created by PhpStorm.
 * User: Quentin
 * Date: 27/02/2017
 * Time: 16:45
 */
class Character extends \Illuminate\Database\Eloquent\Model
{
    protected $table = "character";
    protected $primaryKey = "id";
    public $timestamps = false;


    public function games($idcharacter){
        return $this->belongsToMany('\gamepedia\models\Game','game2character','character_id','game_id');
    }


    public function giveGames($idcompany){
        $c = Company::find($idcompany);
        return $c->games()->get();
    }





}