<?php


namespace gamepedia\models;

/**
 * Created by PhpStorm.
 * User: Quentin
 * Date: 27/02/2017
 * Time: 16:45
 */
class Commentaires extends \Illuminate\Database\Eloquent\Model
{

    protected $table = "commentaires";
    protected $primaryKey = "id";
    public $timestamps = true;


    public function jeu(){
        return  $this->belongsTo('gamepedia\moedls\Usr','id_game');
    }

    public function usr(){
        return $this->belongsTo('gamepedia\models\Usr','email');
    }


    public static function giveComments($idgame){
        return Commentaires::select('email','id','titre','contenu','created_at','updated_at')->where('id_game','=',$idgame)->get();
    }
}